import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: [`
   .users li { cursor: default; }
  .users li:hover { background: #ecf0f1; } 
 
 `]
})
export class UsersComponent implements OnInit {

 users = [
{name:'ray',email:'Ray@gmail.com',phone:'0546963430'},
{name:'michael',email:'Michael@gmail.com',phone:'0546963430'},
{name:'sam',email:'Sam@gmail.com',phone:'0546963430'},
{name:'shay',email:'shay@gmail.com',phone:'05464444'},

  ]
  constructor() { }

  ngOnInit() {
  }

}
